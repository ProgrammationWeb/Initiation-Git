all:
	@pandoc -t html5 --template=template.html --standalone --toc --toc-depth=1 --section-divs --variable css="styles.css" --variable css="reveal.js/css/theme/black.css" --variable transition="slide" Initiation-Git.md -o Initiation-Git.html
	@pandoc --from=markdown+yaml_metadata_block --latex-engine=xelatex --template ./template.latex --chapters --listings -o Initiation-Git.pdf Initiation-Git.md
