Reveal.addEventListener ('ready', moveTitle);
Reveal.addEventListener ('ready', tweakStyles);

Reveal.addEventListener ('slidechanged', moveTitle);

$(window).on ('resize', tweakStyles);

// Move title to top of page
function moveTitle () {
	$('div.main-title-container').empty ();
	if (!$('section.present').is (':first-child')) {
		var $node = $('div.slides>section.present h1').clone ()
			.css ({
				'font-size': $('div.slides>section.present>section.present h2').css ('font-size')
			});
		$('div.main-title-container').empty ().append ($node);
	}
}

function tweakStyles () {
	$('div.main-title-container').css ('transform', 'scale(1,' + Reveal.getScale () + ')');
	if (Reveal.getScale () > 1) {
		$('figure').css ('transform', 'scale('+ (1/Reveal.getScale ()) + ')');
	}
}
