---
    title: Initiation à Git
    subtitle: 
    keywords: 
    author: Sébastien NOBILI
    url: https://gitlab.com/ProgrammationWeb/Initiation-Git
    date: septembre 2017
---

# Généralités

## Pour quoi faire ?

- travailler à plusieurs
- suivre les évolutions du code

## Github, Gitlab ?

- plateformes d’hébergement
- ces outils ne sont pas indispensables

## Ligne de commande ou graphique ?

L’un ou l’autre, tant qu’on maîtrise ce qu’on fait !

# Préparer l’environnement

## Identité

```
git config --global user.name "Votre NOM"
git config --global user.email "votre@dresse.mail"
```

## Couleurs

```
git config --global color.diff auto
git config --global color.status auto
```

## Avant de rentrer chez soi...

On a configuré le poste de travail sur lequel on se trouvait.

*Penser à recopier cette configuration sur les autres ordinateurs utilisés.*

(Sous Linux, la configuration est enregistrée dans « `~/.gitconfig` ».)

# Récupérer du code

## Clonage

```
git clone https://gitlab.com/ProgrammationWeb/Initiation-Git.git
```

▶ on récupère une copie locale d’un dépôt distant

## {-}

```
cd Initiation-Git/
```

## Historique {#git-log}

```
git log
```

▶ on voit la liste les modifications apportées au dépôt :

- leur identifiant unique,
- leur auteur,
- la date,
- le commentaire.

## Détails d’une modification

```
git show c21266bf4006862c0595a0e409719d0da16d816b
```

▶ on voit le contenu de l’enregistrement :

- les lignes ajoutées,
- les lignes supprimées

# Modifier du code

## {-}

```
echo "\n\nMODIFICATION" >> README.md
touch fichier-non-suivi
```

## État des fichiers {#git-status}

```
git status
```

▶ on voit la liste des différents états :

- fichiers suivis modifiés
- fichiers non-suivis ajoutés

## Modifications d’un fichier

```
git diff README.md
```

▶ on retrouve les modifications faites précédemment sur le fichier.

# Modifier le dépôt

## Deux étapes

L’opération se fait en deux étapes :

- on prépare la liste des changements,
- on valide les changements.

*La validation est purement locale, les données ne sont pas encore publiées.*

## Ajouter des modifications

```
git add README.md
```

▶ les modifications du fichier sont prêtes à valider.

*On peut le vérifier par la commande « [git status](#git-status) ».*

## Ajouter un nouveau fichier

```
git add fichier-non-suivi
```

▶ l’ajout du fichier est prêt à valider.

*On peut le vérifier par la commande « [git status](#git-status) ».*

## Suppression d’un fichier existant

```
git rm styles.css
```

▶ la suppression du fichier est prête à valider.

*On peut le vérifier par la commande « [git status](#git-status) ».*

## Valider les modifications

```
git commit -m "Toujours mettre un message explicite"
```

▶ un nouvel enregistrement est créé dans le dépôt.

*On peut le vérifier par la commande « [git log](#git-log) ».*

# Interagir avec le dépôt distant

## Objectif

- synchronisation dépôt local / dépôt distant
- récupérer le travail des autres
- mettre son travail à disposition des autres

## Récupérer les évolutions

```
git pull
```

▶ on obtient le travail réalisé par d’autres depuis le clonage.

**Attention :**

- risque de conflits
- création d’une ramification dans le dépôt

## Publier des modifications

```
git push
```

▶ on envoie nos modifications dans le dépôt distant.

**Attention :**

- droits d’accès au dépôt
- évolutions dans le dépôt distant

# Naviguer dans l’historique

## Créer un tag

```
git tag v1.0
```

▶ on a créé un point de repère dans le dépôt.

**Attention :** par défaut les tags ne sont pas poussés dans le dépôt distant.

## Lister les tags

```
git tag
```

## Extraire une version antérieure

```
git checkout c21266bf4006862c0595a0e409719d0da16d816b
git checkout v1.0
```

▶ on a récupéré une version antérieure du code.

*Les versions suivantes sont toujours disponibles dans le dépôt.*

## Extraire la pointe du dépôt

```
git checkout master
```

# Travailler dans une branche

## Créer une branche

```
git checkout -b fonctionnalite_1
```

▶ on a créé une branche différente de « `master` ».

**Intérêt :**

- on ne perturbe pas les gens qui travaillent sur « `master` »
- on peut retourner sur « `master` » pendant le travail sur « `fonctionnalite_1` »

## Modifier la branche

Les modifications habituelles sont possibles :

- ajout
- suppression
- commit

## Retourner sur la branche principale

```
git checkout master
```

▶ on ne voit plus les modifications faites précédemment.

Retourner sur la branche secondaire

```
git checkout fonctionnalite1
```

▶ on retrouve les modifications.

## Fusionner les branches

```
git checkout master
git merge fonctionnalite1
```

▶ les modifications de « `fonctionnalite1` » sont intégrées à « `master` ».

**Attention :**

- risque de conflits
- création d’un commit de fusion

# Éviter les erreurs

## Ignorer des fichiers

```
echo "fichier_a_ignorer" >> .gitignore
git add .gitignore
git commit -m "Ignorer le fichier \"fichier_a_ignorer\""
```

▶ on ne sera pas tenté d’ajouter le fichier au dépôt.

## Éviter les commandes automatiques

```
git commit -a
```

▶ on valide toutes les modifications !

Et d’ailleurs, tout quoi ???

## Relire ses modifications

```
git diff
```

▶ à lancer avant tout commit !

## Optimiser les commits

Un commit doit être :

- autosuffisant
- complet
- le plus petit possible

## Lire le manuel

```
git help <commande>
```

▶ mieux vaut lire le manuel avant de faire une erreur qu’après !

## Faire une sauvegarde de son travail

- copier le dossier de travail
- faire une archive du dossier de travail

▶ on peut alors tout casser et revenir en arrière !

# Aller plus loin

## Dépôt distant

```
git remote -v
```

- il n’y a pas forcément de dépôt distant
- il peut y avoir plusieurs dépôts distants

## Remiser du code

```
git stash save
```

▶ on met nos modifications de coté.

```
git stash pop
```

▶ on récupère les modifications mises de coté.

**Attention :**

- risque de conflits

## Réécrire l’historique

```
git rebase -i origin master
```

▶ on peut modifier tous les commits qui n’ont pas encore été poussés.

## Rechercher un bug

```
git bisect start
git bisect bad
git bisect good <commit ou tag>
git bisect reset
```

▶ Git nous aide à trouver le commit ayant introduit un bug.

# Documentation

- [https://git-scm.com/book/fr/](https://git-scm.com/book/fr/)
- [https://github.com/progit/progit2](https://github.com/progit/progit2)
- [https://education.github.com/git-cheat-sheet-education.pdf](https://education.github.com/git-cheat-sheet-education.pdf)

---
history:
  - version: "0.1A"
    date: 2017-09-29
    author: Sébastien NOBILI
    description: Version initiale
---
