# Initiation à Git

Diaporama de l’initiation à Git dans le cadre du cours de programmation Web à l'Université Paris Sud.

## Récupération du code

> `git clone --recursive https://gitlab.com/ProgrammationWeb/Initiation-Git.git`

## Construction

> `$ make`

## Lecture

Ouvrir le fichier `Initiation-Git.html` ou `Initiation-Git.pdf` :-)

## Licence

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.
